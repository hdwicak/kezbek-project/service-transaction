import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}

  async sendMail(
    email: string,
    cashbackPromo: number,
    cashbackLoyalty: number,
    cashbackTotal: number,
  ) {
    await this.mailerService.sendMail({
      to: email,
      subject: 'From Kezbek with Love',
      template: './email',
      context: {
        cashbackPromo: cashbackPromo,
        cashbackLoyalty: cashbackLoyalty,
        cashbackTotal: cashbackTotal,
      },
    });
  }
}
