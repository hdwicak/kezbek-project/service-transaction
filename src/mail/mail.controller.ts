import { Controller, Get } from '@nestjs/common';
import { MailService } from './mail.service';

@Controller('mail')
export class MailController {
  constructor(private readonly mailService: MailService) {}

  @Get()
  async sendMail(email, cashbackPromo, cashbackLoyalty, cashbackTotal) {
    await this.mailService
      .sendMail(email, cashbackPromo, cashbackLoyalty, cashbackTotal)
      .catch((err) => console.log(err));
  }
}
