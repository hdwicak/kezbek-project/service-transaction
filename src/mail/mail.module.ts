import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { MailController } from './mail.controller';

import * as dotenv from 'dotenv';
import { join } from 'path';
dotenv.config();

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: process.env.MAIL_SMTP_HOST || 'smtp.mailtrap.io',
        port: process.env.MAIL_SMTP_PORT || 25,
        secure: false,
        auth: {
          user: process.env.MAIL_AUTH_USER,
          pass: process.env.MAIL_AUTH_PASSOWRD,
        },
      },
      defaults: {
        from: '"no-reply" <no-reply@kezbek.com>',
      },
      template: {
        // dir: __dirname + '/mailtemplates',
        dir: join(__dirname, '..', '..', 'mailtemplates'),
        adapter: new EjsAdapter(),
        options: {
          strict: false,
        },
      },
    }),
  ],
  providers: [MailService],
  controllers: [MailController],
})
export class MailModule {}
