import { Injectable } from '@nestjs/common';

import { DataSource, Repository } from 'typeorm';
import { TransactionPayment } from '../entity/transaction-payment.entity';
import { TransactionDataDto } from '../dto/transaction-data.dto';

@Injectable()
export class TransactionPaymentRepository extends Repository<TransactionPayment> {
  constructor(private readonly dataSource: DataSource) {
    super(TransactionPayment, dataSource.createEntityManager());
  }

  async saveTransaction(
    transaction: TransactionDataDto,
  ): Promise<TransactionPayment> {
    return this.save(transaction);
  }
}
