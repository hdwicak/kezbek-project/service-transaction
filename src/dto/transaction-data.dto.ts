import { Type } from 'class-transformer';
import { IsDate } from 'class-validator';

export class TransactionDataDto {
  trx_date: string;

  partner_code: string;

  partner_key: string;

  promo_code: string;

  customer_email: string;

  payment_wallet: string;

  order_id: string;

  purchase_quantity: number;

  purchase_amount: number;

  description: string;

  cashback_promo: number;

  cashback_loyalty: number;

  cashback_total: number;

  id: number;

  created_at: string;

  updated_at: string;
}
