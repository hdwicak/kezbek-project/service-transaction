import { DataSourceOptions } from 'typeorm';
import * as dotenv from 'dotenv';
import { TransactionPayment } from '../entity/transaction-payment.entity';


dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: process.env.MYSQL_HOST || 'localhost',
  port: parseInt(process.env.MYSQL_PORT) || 3307,
  username: process.env.MYSQL_USER || 'admin',
  password: process.env.MYSQL_PASSWD || 'password',
  database: process.env.MYSQL_DB || 'service_transaction',
  entities: [TransactionPayment],
  dateStrings: true,
  synchronize: true,
};
