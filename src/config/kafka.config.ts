import { ClientsModuleOptions, Transport } from '@nestjs/microservices';
import * as dotenv from 'dotenv';

dotenv.config();

export const kafkaOptions: ClientsModuleOptions = [
  {
    name: process.env.KAFKA_TOKEN || 'KAFKA_SERVICE_TRANSACTION',
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: process.env.KAFKA_CLIENT_ID || 'service-transaction',
        brokers: [process.env.KAFKA_BROKERS || 'localhost:19092'],
      },
      consumer: {
        groupId: process.env.KAFKA_GROUPID || 'service-transaction',
      },
    },
  },
];
