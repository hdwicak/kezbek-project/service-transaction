import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class TransactionPayment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'date' })
  trx_date: string;

  @Column()
  partner_code: string;

  @Column()
  partner_key: string;

  @Column()
  promo_code: string;

  @Column()
  customer_email: string;

  @Column()
  payment_wallet: string;

  @Column()
  order_id: string;

  @Column()
  purchase_quantity: number;

  @Column()
  purchase_amount: number;

  @Column()
  description: string;

  @Column()
  cashback_promo: number;

  @Column()
  cashback_loyalty: number;

  @Column()
  cashback_total: number;

  @CreateDateColumn({ type: 'timestamp' })
  payment_date: string;

  @CreateDateColumn({ type: 'timestamp' })
  created_at: string;

  @UpdateDateColumn({ type: 'timestamp' })
  updated_at: string;

  @DeleteDateColumn({ type: 'timestamp' })
  deleted_at: string;
}
