import { Controller, Inject, Logger } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { PaymentService } from './payment.service';
import { lastValueFrom, map } from 'rxjs';
import { MailService } from '../mail/mail.service';
import { TransactionDataDto } from '../dto/transaction-data.dto';

dotenv.config();
@Controller('payment')
export class PaymentController {
  constructor(
    private readonly mailService: MailService,

    @Inject(process.env.KAFKA_TOKEN || 'KAFKA_SERVICE_TRANSACTION')
    private readonly clientKafka: ClientKafka,

    private readonly paymentService: PaymentService,
    private readonly logger: Logger,
  ) {}

  @EventPattern('incoming-service-transaction')
  async makePayment(@Payload() data: TransactionDataDto) {
    this.logger.log(
      '[KEZBEK-SERVICE-TRANSACTION] processing incoming message from topic : incoming-service-transaction',
    );
    const email = data.customer_email;
    const cashbackPromo = data.cashback_promo;
    const cashbackLoyalty = data.cashback_loyalty;
    const cashbackTotal = data.cashback_total;
    const paymentWallet = data.payment_wallet;

    try {
      const url =
        process.env.URL_PAYMENT_SERVICE ||
        'https://givememoney.free.beeceptor.com/send-money';
      //make payment request

      const output = await lastValueFrom(
        (
          await this.paymentService.makePayment(url)
        ).pipe(map((response) => response.data)),
      );

      if (output.status === 'SUCCESS') {
        await this.paymentService.save(data);
        this.logger.log(
          '[KEZBEK-SERVICE-TRANSACTION] successful cashback payment to customer and emit message to topic : success-transaction',
        );
        await this.clientKafka.emit(
          'success-transaction',
          JSON.stringify(data),
        );
      } else {
        this.clientKafka.emit('error-payment', JSON.stringify(data));
        this.logger.log(
          '[FAILED-KEZBEK-SERVICE-TRANSACTION] FAILED cashback payment to customer, emit message to topic : error-payment',
        );
      }
      await this.mailService.sendMail(
        email,
        cashbackPromo,
        cashbackLoyalty,
        cashbackTotal,
      );
    } catch (err) {
      this.logger.error(err);
    }
  }
}
