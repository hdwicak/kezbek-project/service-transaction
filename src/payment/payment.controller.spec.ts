import { Test, TestingModule } from '@nestjs/testing';
import { PaymentController } from './payment.controller';
import { MailService } from '../mail/mail.service';
import { MailerService } from '@nestjs-modules/mailer';
import { PaymentService } from './payment.service';
import { HttpService } from '@nestjs/axios';
import { TransactionPaymentRepository } from '../repository/transaction-payment.repository';
import { Logger } from '@nestjs/common';

describe('PaymentController', () => {
  let controller: PaymentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaymentController],
      providers: [
        PaymentService,
        {
          provide: MailService,
          useValue: {
            sendMail: jest.fn(),
          },
        },
        {
          provide: HttpService,
          useValue: {
            post: jest.fn(),
          },
        },
        {
          provide: TransactionPaymentRepository,
          useValue: {
            save: jest.fn(),
          },
        },
        {
          provide: Logger,
          useValue: {
            log: jest.fn(),
          },
        },
        {
          provide: 'KAFKA_SERVICE_TRANSACTION',
          useValue: {
            emit: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<PaymentController>(PaymentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
