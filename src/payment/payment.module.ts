import { Logger, Module } from '@nestjs/common';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from 'src/config/kafka.config';
import { HttpModule, HttpService } from '@nestjs/axios';
import { MailService } from 'src/mail/mail.service';
import { TransactionPaymentRepository } from '../repository/transaction-payment.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([]),
    ClientsModule.register(kafkaOptions),
    HttpModule,
  ],
  controllers: [PaymentController],
  providers: [
    PaymentService,
    MailService,
    Logger,
    TransactionPaymentRepository,
  ],
  exports: [],
})
export class PaymentModule {}
