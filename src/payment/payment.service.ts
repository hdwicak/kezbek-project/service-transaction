import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { TransactionPayment } from '../entity/transaction-payment.entity';
import { TransactionDataDto } from '../dto/transaction-data.dto';
import { TransactionPaymentRepository } from '../repository/transaction-payment.repository';

@Injectable()
export class PaymentService {
  constructor(
    private readonly httpService: HttpService,
    private readonly transactionRepo: TransactionPaymentRepository
  ) {}

  async makePayment(url: string): Promise<Observable<any>> {
    return this.httpService.post(url);
  }

  async save(transaction: TransactionDataDto): Promise<TransactionPayment> {
    return this.transactionRepo.save(transaction);
  }
}
